from workers import Manager, Programmer, TechnicalWorker
import validation
import pytest


def test_Programmer_construct_valid():
    Adam = Programmer("Adam", "Adamski", "test@gmail.com", 5000, "C++")

    assert str(Adam) == ("Adam Adamski (test@gmail.com), "
                         "5000pln/mth, C++ programmer")


def test_Technical_Worker_construct_valid():
    Czarek = TechnicalWorker("Czarek", "C", "c.c@cc.pl", 70, 40)

    assert str(Czarek) == ("Czarek C (c.c@cc.pl), 40h/mth * 70pln/h, "
                           "technical worker")


def test_Manager_construct_valid():
    Adam = Programmer("Adam", "Adamski", "test@gmail.com", 5000, "C++")
    Czarek = TechnicalWorker("Czarek", "C", "c.c@cc.pl", 70, 40)
    Bartek = Manager("Bartek", "B", "a.a@aa.pl", 10000, [Adam, Czarek])

    assert str(Bartek) == (
        "Bartek B (a.a@aa.pl), 10000pln/mth, manager of:\n"
        " - Adam Adamski (test@gmail.com), 5000pln/mth, C++ programmer\n"
        " - Czarek C (c.c@cc.pl), 40h/mth * 70pln/h, technical worker"
    )


def test_Programmer_construct_invalid_1():
    args = ["Adam", "Adamski", "test@gmail.com", 5000, "C++"]
    for i in range(5):
        with pytest.raises(TypeError):
            Programmer(*(args[:i] + ["A"] + args[i+1:]))
            Programmer(*(args[:i] + [500] + args[i+1:]))


def test_Programmer_construct_invalid_2():
    args = ["Adam", "Adamski", "test@gmail.com", 5000, "C++"]
    for i in range(5):
        if isinstance(args[i], str):
            with pytest.raises(validation.EmptyError):
                Programmer(*(args[:i] + [""] + args[i+1:]))


def test_Programmer_construct_negative_salary():
    with pytest.raises(validation.NegativeError):
        Programmer("Adam", "Adamski", "test@gmail.com", -5000, "C++")


def test_TechnicalWorker_construct_invalid_1():
    args = ["Czarek", "C", "c.c@cc.pl", 70, 40]
    for i in range(5):
        with pytest.raises(TypeError):
            TechnicalWorker(*(args[:i] + ["A"] + args[i+1:]))
            TechnicalWorker(*(args[:i] + [500] + args[i+1:]))


def test_TechnicalWorker_construct_invalid_2():
    args = ["Czarek", "C", "c.c@cc.pl", 70, 40]
    for i in range(5):
        if isinstance(args[i], str):
            with pytest.raises(validation.EmptyError):
                TechnicalWorker(*(args[:i] + [""] + args[i+1:]))


def test_TechnicalWorker_construct_negative_salary():
    with pytest.raises(validation.NegativeError):
        TechnicalWorker("Czarek", "C", "c.c@cc.pl", -70, 40)
    with pytest.raises(validation.NegativeError):
        TechnicalWorker("Czarek", "C", "c.c@cc.pl", 70, -40)


def test_TechnicalWorker_calculate_salary():
    Czarek = TechnicalWorker("Czarek", "C", "c.c@cc.pl", 70, 40)

    assert Czarek.calculate_monthly_income() == 2800


def test_Manager_construct_invalid_1():
    args = ["Bartek", "B", "a.a@aa.pl", 10000, []]
    for i in range(5):
        with pytest.raises(TypeError):
            Manager(*(args[:i] + ["A"] + args[i+1:]))
            Manager(*(args[:i] + [500] + args[i+1:]))


def test_Manager_construct_invalid_2():
    args = ["Bartek", "B", "a.a@aa.pl", 10000, []]
    for i in range(5):
        if isinstance(args[i], str):
            with pytest.raises(validation.EmptyError):
                Manager(*(args[:i] + [""] + args[i+1:]))


def test_Manager_add_worker():
    Adam = Programmer("Adam", "Adamski", "test@gmail.com", 5000, "C++")
    Czarek = TechnicalWorker("Czarek", "C", "c.c@cc.pl", 70, 40)
    Bartek = Manager("Bartek", "B", "a.a@aa.pl", 10000, [Adam])

    Bartek.add_worker(Czarek)

    assert str(Bartek) == (
        "Bartek B (a.a@aa.pl), 10000pln/mth, manager of:\n"
        " - Adam Adamski (test@gmail.com), 5000pln/mth, C++ programmer\n"
        " - Czarek C (c.c@cc.pl), 40h/mth * 70pln/h, technical worker"
    )


def test_Manager_add_worker_2():
    Adam = Programmer("Adam", "Adamski", "test@gmail.com", 5000, "C++")
    Czarek = TechnicalWorker("Czarek", "C", "c.c@cc.pl", 70, 40)
    Bartek = Manager("Bartek", "B", "a.a@aa.pl", 10000, [])

    Bartek.add_worker(Czarek)
    Bartek.add_worker(Adam)

    assert str(Bartek) == (
        "Bartek B (a.a@aa.pl), 10000pln/mth, manager of:\n"
        " - Czarek C (c.c@cc.pl), 40h/mth * 70pln/h, technical worker\n"
        " - Adam Adamski (test@gmail.com), 5000pln/mth, C++ programmer"
    )


def test_Manager_add_worker_invalid():
    Bartek = Manager("Bartek", "B", "a.a@aa.pl", 10000, [])

    with pytest.raises(TypeError):
        Bartek.add_worker("Czekolada")


def test_Manager_remove_worker():
    Adam = Programmer("Adam", "Adamski", "test@gmail.com", 5000, "C++")
    Czarek = TechnicalWorker("Czarek", "C", "c.c@cc.pl", 70, 40)
    Bartek = Manager("Bartek", "B", "a.a@aa.pl", 10000, [Adam, Czarek])

    assert str(Bartek) == (
        "Bartek B (a.a@aa.pl), 10000pln/mth, manager of:\n"
        " - Adam Adamski (test@gmail.com), 5000pln/mth, C++ programmer\n"
        " - Czarek C (c.c@cc.pl), 40h/mth * 70pln/h, technical worker"
    )

    Bartek.remove_worker("Adam", "Adamski", "test@gmail.com")

    assert str(Bartek) == (
        "Bartek B (a.a@aa.pl), 10000pln/mth, manager of:\n"
        " - Czarek C (c.c@cc.pl), 40h/mth * 70pln/h, technical worker"
    )


def test_Manager_remove_worker_not_added():
    Czarek = TechnicalWorker("Czarek", "C", "c.c@cc.pl", 70, 40)
    Bartek = Manager("Bartek", "B", "a.a@aa.pl", 10000, [Czarek])

    with pytest.raises(ValueError):
        Bartek.remove_worker("Adam", "Adamski", "test@gmail.com")
