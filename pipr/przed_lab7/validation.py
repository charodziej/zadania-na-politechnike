class EmptyError(ValueError):
    def __init__(self):
        super().__init__("Supplied string can't be empty")


class NegativeError(ValueError):
    def __init__(self, name):
        super().__init__(f"{name} can't be negative")


def check_string(var):
    return isinstance(var, str)


def check_number(var):
    return isinstance(var, int) or isinstance(var, float)
