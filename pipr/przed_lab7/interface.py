from workers import Manager, Programmer, TechnicalWorker, Worker


class StopException(Exception):
    def __init__(self):
        super().__init__("Halt")


class Interface:
    def __init__(self):
        self.people = []

    def print_options(self):
        print(
            "Select one of the following operations:\n"
            " 0) list people\n"
            " 1) add a person\n"
            " 2) remove a person\n"
            " 3) edit basic details\n"
            " 9) exit"
        )

    def get_person_info(self):
        print("Specify name:")
        name = str(input())
        print("Specify surname:")
        surname = str(input())
        print("Specify email:")
        email = str(input())
        return name, surname, email

    def list_people(self):
        for index, worker in enumerate(self.people):
            print(f" {index:>3}: {worker}")

    def add_person(self):
        print(
            "Select worker type:\n"
            " 0) manager\n"
            " 1) programmer\n"
            " 2) techincal worker\n"
        )
        try:
            selection = int(input())
            if (0 > selection or 2 < selection):
                raise ValueError

            name, surname, email = self.get_person_info()

            if selection == 0:
                print("Specify salary:")
                salary = int(input())

                print(name, surname, email, salary)
                self.people.append(Manager(name, surname, email, salary, []))
            elif selection == 1:
                print("Specify salary:")
                salary = int(input())
                print("Specify programming language:")
                language = str(input())
                self.people.append(Programmer(name, surname, email,
                                              salary, language))
            elif selection == 2:
                print("Specify price per hour of work:")
                price_per_hour = int(input())
                print("Specify monthly hours worked:")
                monthly_hours = int(input())
                self.people.append(
                    TechnicalWorker(name, surname,
                                    email, price_per_hour, monthly_hours))
        except:
            print("Creation unsuccessful, aborting...")

    def remove_person(self):
        name, surname, email = self.get_person_info()
        try:
            self.people.remove(Worker(name, surname, email))
            for worker in self.people:
                if isinstance(worker, Manager):
                    try:
                        worker.remove_worker(name, surname, email)
                    except ValueError:
                        continue
        except:
            print("Deletion unsuccessful, aborting...")

    def edit_basic_details(self):
        name, surname, email = self.get_person_info()
        try:
            index = self.people.index(Worker(name, surname, email))

            print("Specify new name:")
            name = str(input())
            print("Specify new surname:")
            surname = str(input())
            print("Specify new email:")
            email = str(input())

            self.people[index].set_name(name)
            self.people[index].set_surname(surname)
            self.people[index].set_email(email)
        except:
            print("Edition unsuccessful, aborting...")

    def exit(self):
        raise StopException

    def get_input(self):
        while True:
            try:
                data = int(input())
                if data not in [0, 1, 2, 3, 9]:
                    raise ValueError
                return data
            except:
                print("Please supply a valid action")

    def tick(self):
        self.print_options()
        selection = self.get_input()

        dict({
            0: self.list_people,
            1: self.add_person,
            2: self.remove_person,
            3: self.edit_basic_details,
            9: self.exit
        }).get(selection, lambda: None)()
