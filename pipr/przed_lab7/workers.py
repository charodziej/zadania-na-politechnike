from validation import check_number, check_string, EmptyError, NegativeError


class Worker:
    def __init__(self, name, surname, email):
        self.set_name(name)
        self.set_surname(surname)
        self.set_email(email)

    def name(self):
        return self._name

    def set_name(self, name):
        if not check_string(name):
            raise TypeError
        if name == "":
            raise EmptyError
        self._name = name

    def surname(self):
        return self._surname

    def set_surname(self, surname):
        if not check_string(surname):
            raise TypeError
        if surname == "":
            raise EmptyError
        self._surname = surname

    def email(self):
        return self._email

    def set_email(self, email):
        if not check_string(email):
            raise TypeError
        if email == "":
            raise EmptyError
        self._email = email

    def __eq__(self, arg):
        return (
            self._name == arg.name() and
            self._surname == arg.surname() and
            self._email == arg.email()
        )

    def __str__(self):
        return f"{self._name} {self._surname} ({self._email})"


class EtatWorker(Worker):
    def __init__(self, name, surname, email, salary):
        super().__init__(name, surname, email)
        self.set_salary(salary)

    def salary(self):
        return self._salary

    def set_salary(self, salary):
        if not check_number(salary):
            raise TypeError
        if salary < 0:
            raise NegativeError("Salary")
        self._salary = salary

    def __str__(self):
        return super().__str__() + f", {self._salary}pln/mth"


class HourlyWorker(Worker):
    def __init__(self, name, surname, email, price_per_hour, monthly_hours):
        super().__init__(name, surname, email)
        self.set_price_per_hour(price_per_hour)
        self.set_monthly_hours(monthly_hours)

    def price_per_hour(self):
        return self._price_per_hour

    def set_price_per_hour(self, price_per_hour):
        if not check_number(price_per_hour):
            raise TypeError
        if price_per_hour < 0:
            raise NegativeError("Pay per hour")
        self._price_per_hour = price_per_hour

    def monthly_hours(self):
        return self._monthly_hours

    def set_monthly_hours(self, monthly_hours):
        if not check_number(monthly_hours):
            raise TypeError
        if monthly_hours < 0:
            raise NegativeError("Monthly hours")
        self._monthly_hours = monthly_hours

    def calculate_monthly_income(self):
        return self._price_per_hour * self._monthly_hours

    def __str__(self):
        return (
            super().__str__() +
            f", {self._monthly_hours}h/mth * {self._price_per_hour}pln/h"
        )


class Programmer(EtatWorker):
    def __init__(self, name, surname, email, salary, language):
        super().__init__(name, surname, email, salary)
        self.set_language(language)

    def language(self):
        return self._language

    def set_language(self, language):
        if not check_string(language):
            raise TypeError
        if language == "":
            raise EmptyError
        self._language = language

    def __str__(self):
        return super().__str__() + f", {self._language} programmer"


class Manager(EtatWorker):
    def __init__(self, name, surname, email, salary, workers):
        super().__init__(name, surname, email, salary)
        self.set_workers(workers)

    def workers(self):
        return self._workers

    def set_workers(self, workers):
        if (not isinstance(workers, list) or
                any([not isinstance(worker, Worker) for worker in workers])):
            raise TypeError

        self._workers = workers

    def add_worker(self, worker):
        if not isinstance(worker, Worker):
            raise TypeError
        self._workers.append(worker)

    def remove_worker(self, name, surname, email):
        self._workers.remove(Worker(name, surname, email))

    def __str__(self):
        result = super().__str__() + ", manager of:"
        for worker in self._workers:
            result += f"\n - {worker}"
        return result


class TechnicalWorker(HourlyWorker):
    def __init__(self, name, surname, email, price_per_hour, monthly_hours):
        super().__init__(name, surname, email, price_per_hour, monthly_hours)

    def __str__(self):
        return super().__str__() + ", technical worker"
