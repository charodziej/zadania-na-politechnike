from recipe import Recipe, Menu, get_recipes
from secrets import app_id, app_key


test_recipes_list = [
    {"label": "Kapusta",
     "ingredientLines": "Kapusta",
     "calories": 100,
     "yield": 5},
    {"label": "Pierogi",
     "ingredientLines": "maka, woda, farsz",
     "calories": 400,
     "yield": 2},
    {"label": "Barszcz",
     "ingredientLines": "buraki, woda, uszka",
     "calories": 500,
     "yield": 5}
]


def test_recipe():
    recipe = Recipe(test_recipes_list[0])
    assert recipe._name == "Kapusta"


def test_get_kcal_per_portion():
    recipe = Recipe(test_recipes_list[0])
    assert recipe.get_kcal_per_portion() == 20


def test_add_recipe():
    menu = Menu()
    recipe = Recipe(test_recipes_list[0])
    menu.add_recipe(recipe)
    assert menu.recipes[0]._name == "Kapusta"


def test_get_num_dishes():
    recipes = [Recipe(test_recipe) for test_recipe in test_recipes_list]
    menu = Menu(recipes)
    assert menu.get_num_dishes() == 3


def test_get_recipes():
    recipes = get_recipes("kapusta", app_id=app_id, app_key=app_key)
    assert len(recipes) == 20
    assert recipes[2]._name == "Polish Kapusta"
