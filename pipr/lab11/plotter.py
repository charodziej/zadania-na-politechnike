from matplotlib import pyplot as plt
import recipe


def sort_recipes(calories, labels):
    data = list(zip(calories, labels))
    data.sort(key=lambda dish: dish[0], reverse=True)
    calories, labels = list(zip(*data))
    return calories, labels


def plot_menu(menu):
    labels = []
    calories = []
    for menu_recipe in menu.recipes:
        labels.append(str(menu_recipe))
        calories.append(menu_recipe.get_kcal_per_portion())
    calories, labels = sort_recipes(calories, labels)

    fig1, ax1 = plt.subplots()
    ax1.pie(calories, labels=labels, autopct='%1.1f%%',
            shadow=True, startangle=90)

    title = f"Wigilijne kalorie: {sum(calories)}"
    plt.title(title, fontdict={"fontsize": 'xx-large'})
    plt.show()


if __name__ == "__main__":
    test_recipes = [
        recipe.Recipe({"label": "Kapusta",
                       "ingredientLines": "Kapusta",
                       "calories": 100,
                       "yield": 5}),
        recipe.Recipe({"label": "Pierogi",
                       "ingredientLines": "maka, woda, farsz",
                       "calories": 400,
                       "yield": 2}),
        recipe.Recipe({"label": "Barszcz",
                       "ingredientLines": "buraki, woda, uszka",
                       "calories": 500,
                       "yield": 5})
    ]
    test_menu = recipe.Menu(test_recipes)
    plot_menu(test_menu)
