import requests
import plotter
from secrets import app_id, app_key


url = "https://api.edamam.com/api/recipes/v2"


def get_recipes(query, app_id, app_key):
    response = requests.get(url, params={
            "q": query,
            "app_id": app_id,
            "app_key": app_key,
            "type": "public"
        }).json()
    return [Recipe(data['recipe']) for data in response['hits']]


class Recipe:
    def __init__(self, data):
        self._name = data["label"]
        self._ingredients = data["ingredientLines"]
        self._kcal = data["calories"]
        self._portions = data["yield"]

    def get_kcal_per_portion(self):
        return self._kcal / self._portions

    def __str__(self):
        return self._name


class Menu:
    def __init__(self, recipes=None):
        self.recipes = recipes if recipes is not None else []

    def add_recipe(self, recipe):
        self.recipes.append(recipe)

    def get_menu_list(self):
        output_string = ""
        for idx, recipe in enumerate(self.recipes):
            output_string += f"{idx}. {recipe}\n"
        return output_string

    def get_num_dishes(self):
        return len(self.recipes)

    def __str__(self):
        return str(self.get_menu_list())


def get_selection(limit):
    while True:
        try:
            selection = int(input())
            if selection < 0 or selection > limit:
                print("Invalid input")
            else:
                return selection
        except ValueError:
            print("Invalid input")


def main():
    menu = Menu()
    n_dishes = 3
    while n_dishes > 0:
        print("What would You like to eat?")
        query = input()
        tmp_menu = Menu(get_recipes(query, app_id, app_key))
        if tmp_menu.get_num_dishes() == 0:
            print("No recipe with given name")
            continue
        print(tmp_menu)

        print("Please select dish number")
        selection = get_selection(tmp_menu.get_num_dishes())

        menu.add_recipe(tmp_menu.recipes[selection])
        n_dishes -= 1
    print("Final menu:")
    print(menu)
    plotter.plot_menu(menu)


if __name__ == "__main__":
    main()
